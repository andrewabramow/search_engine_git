#include <iostream>
#include <fstream>
#include <iomanip>
#include "JSON.h"
#include "nlohmann/json.hpp"

std::vector <std::string> ConverterJSON::GetTextDocuments() {

	std::ifstream f("..\\config.json");  // open config file

	nlohmann::json j;  // create new json object

	std::vector<std::string> documents_paths;

	std::vector<std::string> text_documents;

	//  read docs paths from config
	if (f.is_open())
	{
		f >> j;  // parse configs to json object

		if (!j.is_null()) 
		{

			// check engine name
			std::cout << "Engine name: " << j["config"]["name"] << std::endl

				// check engine version
				<< "Version: " << j["config"]["version"] << std::endl

				// check max responses
				<< "Max responses: " << j["config"]["max_responses"] << std::endl;

			for (auto& el : j["files"]) 
			{
				// collect texts from request in vector
				std::string contents;

				std::ifstream ifs(el);

				contents.assign((std::istreambuf_iterator<char>(ifs)),
					(std::istreambuf_iterator<char>()));

				// all letters are lowercase
				std::transform(contents.begin(),
							   contents.end(), 
							   contents.begin(), 
							   ::tolower);

				text_documents.push_back(contents);

				ifs.close();
			}
		}
		else 
		{
			std::cerr << " config file is empty" << std::endl;
		}
	}
	else 
	{
		std::cerr << " config file is missing" << std::endl;
	}

	return text_documents;
}

int ConverterJSON::GetResponsesLimit() 
{

	std::ifstream f("..\\config.json");  // open config file

	nlohmann::json j;  // create new json object

	f >> j;  // parse configs to json object

	int max_responses{ 0 };

	if (f.is_open()) 
	{
		if (!j.is_null()) 
		{
			max_responses = j["config"]["max_responses"];
		}
		else 
		{
			std::cerr << " config file is empty" << std::endl;
		}
	}
	else 
	{
		std::cerr << " config file is missing" << std::endl;
	}

	return max_responses;
}

std::vector<std::string> ConverterJSON::GetRequests() 
{

	std::ifstream f("..\\requests.json");  // open requests file

	nlohmann::json j;  // create new json object

	std::vector<std::string> requests;

	if (f.is_open())
	{
		f >> j;  // parse configs to json object

		if (!j.is_null()) {

			for (auto& el : j["requests"]) 
			{
				// collect requests in vector
				requests.push_back(el);
			}
		}
		else 
		{
			std::cerr << " config file is empty" << std::endl;
		}
	}
	else 
	{
		std::cerr << " config file is missing" << std::endl;
	}

	return requests;
}

std::string RequestNumber(int n) 
{
	std::string numb = std::to_string(n);

	while (numb.size() != 3) numb = '0' + numb;

	return numb;
}

double ToTwoDecimalPlaces(float f) 
{
	int i;

	if (f >= 0)
		i = static_cast<int>(f * 100 + 0.5);
	else
		i = static_cast<int>(f * 100 - 0.5);

	return (i / 100.0);
}

void ConverterJSON::putAnswers(std::vector<std::vector<std::pair<int, float>>>
	answers) 
{
	std::ofstream f("../answers.json");

	nlohmann::json jAnswers;

	int counter = 0;

	// parse input vector
	for (auto& El : answers) 
	{
		std::cout << counter + 1 << " answer write\n";

		nlohmann::json jRequest;

		if (El.size() > 0) 
		{
			jRequest["result"] = true;

			nlohmann::json docid_rank;

			for (int i = 0; i < GetResponsesLimit();i++) 
			{
				nlohmann::json temp;

				temp["docid"] = El[i].first;

				temp["rank"] = ToTwoDecimalPlaces(El[i].second);

				docid_rank.push_back(temp);
			}
			jRequest["relevance"] = docid_rank;

			counter++;

			jAnswers["request" + RequestNumber(counter)] = jRequest;
		}
		else 
		{
			jRequest["result"] = false;

			counter++;

			jAnswers["request" + RequestNumber(counter)] = jRequest;
		}
	}
	// collect all intermediate json objects
	nlohmann::json output;

	output["answers"] = jAnswers;

	if (f.is_open()) 
	{
		f << std::setw(4) << output;
	}
	f.close();
}
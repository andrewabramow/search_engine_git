#pragma once

#include <iostream>
#include <vector>
#include <map>

struct Entry 
{
	size_t doc_id, count;
	
	// ������ �������� ��������� ��� ���������� �������� ���������
	bool operator ==(const Entry& other) const 
	{
		return (doc_id == other.doc_id &&
			count == other.count);
	}
};

std::vector<std::string> ParseDocs(std::string line);

size_t Count(std::string word, std::string line);

void Indexing(size_t text_id, std::vector<std::string> docs,
	std::map<std::string, std::vector<Entry>>& freq_dictionary);

class InvertedIndex 
{
public:
	InvertedIndex() = default;
	/*
	* �������� ��� ��������� ���� ����������, �� ������� ����� ���������
	�����
	* @param texts_input ���������� ����������
	*/
	void UpdateDocumentBase(std::vector<std::string> input_docs);
	/**
	* ����� ���������� ���������� ��������� ����� word � ����������� ����
	����������
	* @param word �����, ������� ������������ �������� ���������� ����������
	* @return ���������� �������������� ������ � �������� ����
	*/
	std::vector <Entry> GetWordCount(const std::string& word);

	std::vector<std::string> GetDocs();

	void SetDocs(std::vector<std::string> v);

	std::map<std::string, std::vector<Entry>> GetFreqDictionary();

	void SetFreqDictionary(std::map<std::string, std::vector<Entry>>);

private:
	std::vector<std::string> docs; // ������ ����������� ����������

	std::map<std::string, std::vector<Entry>> freq_dictionary; // ��������� �������
};
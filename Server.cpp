#include <vector>
#include <map>
#include <thread>
#include <mutex>
#include <algorithm>
#include <unordered_set>
#include "InvertIndex.h"
#include "Server.h"

InvertedIndex SearchServer::GetInvertedIndex() 
{
	return _index;
}

size_t MaxCount(std::vector<Entry> freq) 
{
	size_t max_count{ 0 };

	for (auto& el : freq) 
	{
		if (el.count > max_count) max_count = el.count;
	}

	return max_count;
}

void SortByRelev(std::vector<RelativeIndex>& relevance) 
{
	std::sort(relevance.begin(), relevance.end(),
		[](RelativeIndex a, RelativeIndex b) 
		{
		return a.rank > b.rank;
		});
}

void SortLess(std::vector<RelativeIndex>& relevance, int& begin, int& end) 
{
	struct 
	{
		bool operator()(RelativeIndex a, RelativeIndex b) const
		{
			return a.doc_id < b.doc_id;
		}
	} 
	customLess;
	std::sort(relevance.begin() + begin, relevance.begin() + end, customLess);
}

void SortById(std::vector<RelativeIndex>& input) 
{
	int begin{ 0 };

	float init_rank = input[0].rank;

	std::vector<RelativeIndex> output;

	for (int i = 0; i < input.size(); ++i) 
	{
		if (input[i].rank != init_rank || i == input.size() - 1) 
		{
			init_rank = input[i].rank;

			if (i == input.size() - 1)
				i = input.size();

			//  sorting by id part of the input
			SortLess(input, begin, i);

			//  move to next range
			begin = i;
		}
	}
}

void SortByRank(std::vector<RelativeIndex>& input) {

	float init_rank = input[0].rank;

	for (auto it = begin(input); it != end(input); ++it) 
	{
		if (it->rank > init_rank) 
		{
			//init_rank = it->rank;
			auto it_begin = input.begin();

			if (it->rank <= it_begin->rank) 
			{
				it_begin++;
			}
			input.insert(it_begin,(*it));

			it++;

			input.erase(it);
		}
	}
}

std::vector <size_t> Union(std::vector <size_t>& v1,
	std::vector <size_t>& v2) 
{
	std::vector<size_t> answer_docs;

	std::set_union(v1.begin(),
		v1.end(),
		v2.begin(),
		v2.end(),
		std::back_inserter(answer_docs));

	return answer_docs;
}

std::vector<RelativeIndex> SingleSearch(std::string request,
	std::map<std::string,
	std::vector<Entry>>&
	freq_dictionary) 
{
	std::vector<RelativeIndex> relevance;

	// 1. parse request line into words
	std::vector<std::string> parsed_request = ParseDocs(request);

	// 2. unique words with max count
	std::vector<WordOccurrence> word_occurrence;

	for (auto& el : parsed_request) 
	{
		//  freq_dictionary contains this word
		if (freq_dictionary.count(el) > 0) 
		{
			WordOccurrence temp{ el, MaxCount(freq_dictionary[el]) };
			// no repeats

			bool flag = true;

			for (auto& el : word_occurrence) 
			{
				if (el.word == temp.word)
				{
					flag = false;
				}
			}
			if (flag) {
				word_occurrence.push_back(temp);
			}
		}
	}

	std::vector <size_t> answer_docs;

	//  5. vector of doc_id's of requested words
	for (int i = 0; i < word_occurrence.size(); i++) 
	{
		std::vector<size_t> temp;

		for (auto& el : freq_dictionary[word_occurrence[i].word]) 
		{
			temp.push_back(el.doc_id);
			std::cout << word_occurrence[i].word << " - " << el.doc_id << std::endl;
		}
		//  renew answer docs (id's) on every step
		answer_docs = Union(answer_docs, temp);
	}
	//  7. calculate relevance
	for (auto& El : answer_docs) //  answer docs ids
	{  
		size_t count{ 0 };

		for (auto& el : word_occurrence) //  words & counts
		{  
			for (auto& f : freq_dictionary[el.word]) 
			{
				//  search for word count of answer in freq_dict by id
				if (f.doc_id == El) count += f.count;
			}
		}
		relevance.push_back({ El,(float)count });
	}
	/* For now vector of Relative Index contains
	absolute relevance. We'll get relative relevance
	on next step in search function */
	//  Sort by relevance
	SortByRelev(relevance);

	//  Sort by id
	SortById(relevance);

	//relevance.resize(5);
	return relevance;
}

std::vector<std::vector<RelativeIndex>> SearchServer::search(const
	std::vector<std::string>& queries_input) 
{
	//  get freq_dictionary from InvertedIndex object
	std::map<std::string, std::vector<Entry>>freq_dictionary =
		GetInvertedIndex().GetFreqDictionary();

	//  for each of queries_inputs make SingleSearch
	std::vector<std::vector<RelativeIndex>>v_relevance;

	for (auto& el : queries_input) 
	{
		v_relevance.push_back(SingleSearch(el,
			freq_dictionary));
	}

	//  find max absolute relevance for each doc_id
	std::vector<size_t> max_relevance;

	for (auto& El : v_relevance) 
	{
		size_t temp{ 0 };

		for (auto& el : El) 
		{
			if (el.rank > temp) 
			{
				temp = el.rank;
			}
		}
		max_relevance.push_back(temp);
	}
	std::cout << "Max relevance is: \n";
	for (auto& el : max_relevance) 
	{
		std::cout << el << std::endl;
	}
	//  7. relative relevance
	std::vector < std::vector <RelativeIndex>> relative_relevance;

	for (int i = 0; i < v_relevance.size(); i++) 
	{
		std::vector <RelativeIndex > temp;

		for (auto& el : v_relevance[i]) 
		{
			//  no division by zero
			if (max_relevance[i] != 0) 
			{
				temp.push_back({ el.doc_id,el.rank / (float)max_relevance[i] });
			}
		}
		relative_relevance.push_back(temp);
	}

	std::cout << "Absolute relevance is: \n";

	for (auto& El : v_relevance) 
	{
		for (auto& el : El) 
		{
			std::cout << "doc.id - " << el.doc_id
				<< " : rank - " << el.rank << std::endl;
		}
		std::cout << std::endl;
	}

	std::cout << "Relative relevance is: \n";
	for (auto& El : relative_relevance) 
	{
		for (auto& el : El) 
		{
			std::cout << "doc.id - " << el.doc_id
				<< " : rank - " << el.rank << std::endl;
		}
		std::cout << std::endl;
	}

	return relative_relevance;
}
std::vector<std::vector<std::pair<int, float>>> ConvertAnswer(
	const std::vector < std::vector <RelativeIndex>> answer) 
{
	std::vector<std::vector<std::pair<int, float>>> output;

	for (auto& El : answer) 
	{
		std::vector<std::pair<int, float>> temp;

		for (auto& el : El) 
		{
			temp.push_back(std::make_pair(int(el.doc_id), el.rank));
		}
		output.push_back(temp);
	}

	return output;
}
